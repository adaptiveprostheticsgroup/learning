How to install:

I haven't figured out how to properly distribute python modules yet, but this is what I do when I want to install the learning module and still develop on it:

Create the directory ~/.local/lib/python2.7/site-packages if it doesn't already exist. Then cd to that directory. Next do


```
#!bash

ln -s ~/wherever/you/cloned/learning ./learning
```
