import unittest
import numpy as np
from learning.vartogo import VarToGo


"""
python -m unittest discover
"""

class TestVarToGo(unittest.TestCase):

    def test_calc_delta(self):
        delta = VarToGo._calc_delta(beta=0.1, r=2.0, gamma=0.75, primary_bootstrap_prediction=0.2, current_prediction=1.0, bootstrap_prediction=4.0/3.0)
        
        self.assertAlmostEqual(0.64, delta)
        
    def test_calc_traces(self):
        traces = np.array([0.2, 0.1])
        VarToGo._update_traces(traces, gamma=0.75, _lambda=0.75, indices=np.array([0,1]), alpha=0.1)
        
        self.assertTrue(np.allclose([1.095625, 1.039375], traces))
        
    def test_update_weights(self):
        weights = np.array([1.0, 2.0])
        traces = np.array([0.1, 0.2])
        indices = np.array([0, 1])
        
        VarToGo._update_weights(weights, alpha=0.1, delta=0.3, 
                                    current_prediction=0.2, 
                                    last_prediction=-0.1, 
                                    traces=traces, 
                                    last_indices=indices)
        
        self.assertTrue(np.allclose([0.976, 1.982], weights))
        
        
    def test_update(self):
        weights = np.array([1.0, 2.0])
        traces = np.array([0.1, 0.2])
        indices = np.array([0, 1])
        last_indices = np.array([1])
        
        gvf = VarToGo(2, gamma=0.75, l=0.25, instantaneous=True, start_indices=last_indices)
        gvf.weights = weights
        gvf.traces = traces
        gvf.last_value = -0.1
        
        gvf.update(2.0, feature_indices=indices, primary_bootstrap_prediction=0.2)
        
        self.assertTrue(np.allclose(gvf.traces, np.array([0.01875, 1.03375])))
        self.assertAlmostEqual(gvf.last_value, 3.0)
        self.assertTrue(np.allclose(gvf.last_indices, indices))
        self.assertTrue(np.allclose(gvf.weights, np.array([1.006, 2.1208])))
        
        
if __name__=="__main__":
    
    suite = unittest.TestLoader().loadTestsFromTestCase(TestVarToGo)
    unittest.TextTestRunner(verbosity=2).run(suite)