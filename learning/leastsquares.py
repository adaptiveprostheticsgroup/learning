#! usr/bin/env python

import numpy as np

from ros_utils.timed import timed

@timed
def lstd_lambda(l, r_m, phi_m_indices, phi_length):
    """
    phi_m_indices is a list of indices for each sample. Assuming Sparse Binary features.
    If you need something else, modify this function
    
    Algorithm implemented from "Technical update: Least-squares temporal difference learning", J. Boyan, 2002
    """
    if len(phi_m_indices) - 1 > len(r_m):
        raise Exception
    K = phi_length
    A = np.zeros((K, K))
    b = np.zeros(K)

    def sparse_to_binary(sparse, length):
        binary = np.zeros(length)
        binary[sparse] = 1
        return binary
    
    z = sparse_to_binary(phi_m_indices[0], K)
    
    for idx in xrange(len(phi_m_indices) - 1):
        phi_t = sparse_to_binary(phi_m_indices[idx], K)
        phi_t1 = sparse_to_binary(phi_m_indices[idx + 1], K)
        A = A + np.outer(z, (phi_t-phi_t1))
        b = b + z*r_m[idx]
        z = l*z + phi_t1
        
    return np.inner(np.linalg.pinv(A),b)
    
class LSPE(object):
    
    def __init__(self):
        pass
        