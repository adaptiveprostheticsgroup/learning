import numpy as np

class FeatureCounter(object):
    
    def __init__(self, feature_length):
        self.feature_length = feature_length
        self.weights = np.zeros((feature_length))
        
    def increment(self, feature_indices):
        self.weights[feature_indices] += 1
        
    def count(self, feature_indices):
        return np.sum(self.weights)
        