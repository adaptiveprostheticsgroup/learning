#!/usr/bin/env python

import itertools
from learning.tiles2 import *
import numpy as np
import math


class ArrayBuilder:
    def __init__(self, length, dtype=None):
        self.position = 0
        self.array = np.zeros(length, dtype=dtype)

    def append(self, data):
        length = 1
        if isinstance(data, list):
            length = len(data)
        elif isinstance(data, np.ndarray):
            length = data.size

        if length == 0:
            return

        if self.position + length > self.array.size:
            raise IndexError

        np.copyto(self.array[self.position:self.position + length], data)
        self.position += length

    def get_array(self):
        return self.array

    def home(self):
        self.position = 0


# class History:
# def __init__(self, length, resolution = 20):
# self.decay = 0.95
# self.tc = tc.TileCoder(dimension = 1, num_tilings = 1, resolutions=resolution)
# self.tiling_size = self.tc.all_tilings_size
# self.length = length * self.tc.all_tilings_size
# self.history = np.zeros(length)
# self.ab = ArrayBuilder(self.length)
# def update(self, data):
# self.history = np.maximum(data, self.decay*self.history)
# self.ab.home()
# for hist_val in self.history:
# self.ab.append(self.tc.tile_it([hist_val]))

# def get_history(self):
# return self.history
# def get_tiled(self):
# return self.ab.get_array()
# def get_length(self):
# return self.length


class RichTileCoderGroup(object):
    # TODO: rather than creating a new array we could pass in a buffer to write to
    # this should be faster
    # TODO: what are good defaults here that don't overlap
    def __init__(self, dimensions=1, num_tilings=None, resolutions=None, mem_size=None, safetyval="none", seed=0):

        self.tc = Tiles(seed=seed)

        self.dimensions = dimensions
        self.num_tilings = num_tilings or [3, 6]
        self.resolutions = resolutions or [[2], [5]]
        self.mem_size = mem_size or [512]
        self.safetyval = safetyval

        if not safetyval or safetyval == "none":
            self.c_tables = self.mem_size
        else:
            self.c_tables = [CollisionTable(sizeval=m, safetyval=self.safetyval) for m in self.mem_size]

        if len(self.resolutions) != len(self.num_tilings):
            raise Exception("resolutions and num_tilings must be the same length")

        if len(self.mem_size) != len(self.num_tilings):
            raise Exception("mem_size must be the same length as num_tilings")

        for res in resolutions:
            if not isinstance(res, list):
                res = [res]

            if len(res) != dimensions:
                raise Exception("resolution dimensions does not match dimensions")

        self.sparse_size = sum(num_tilings)

        self.buffer = [0] * self.sparse_size
        self.offset_buffer = np.zeros(self.sparse_size, dtype=int)

        offset = 0
        for idx, num in enumerate(num_tilings):
            if idx:
                self.offset_buffer[offset:num] += self.mem_size[idx - 1]

            offset += num

        self.full_size = sum(self.mem_size)

    def tile_it(self, vals, offset=0):
        """
        vals should already be normalized
        """
        # TODO: not thread safe

        buffer_offset = 0
        for idx, num in enumerate(self.num_tilings):
            scaled = (np.array(vals) * np.array(self.resolutions[idx])).tolist()
            self.tc.loadtiles(tiles=self.buffer,
                              startelement=buffer_offset,
                              numtilings=num,
                              memctable=self.c_tables[idx],
                              floats=scaled)
            buffer_offset += num

        return (self.offset_buffer + np.array(self.buffer, dtype=int) + offset).tolist()


class TileCoderGroup:
    # TODO: rather than creating a new array we could pass in a buffer to write to
    # this should be faster
    def __init__(self, dimension=1, num_tilings=[3, 6], resolutions=[2, 5]):
        self.dimension = dimension
        self.num_tilings = num_tilings
        self.resolutions = resolutions
        self.coders = []

        self.all_tilings_size = 0

        assert len(resolutions) == len(num_tilings)

        for idx, res in enumerate(resolutions):
            coder = TileCoder(self.dimension, num_tilings[idx], res)
            self.all_tilings_size += coder.all_tilings_size
            self.coders.append(coder)

        self.full_size = self.all_tilings_size
        self.sparse_size = sum(num_tilings)

    def tile_it(self, vals, offset=0):
        # TODO: should be using numpy
        tiles = []
        for coder in self.coders:
            tiles.extend(coder.tile_indices(vals))

        if offset:
            tiles = (np.array(tiles) + offset).tolist()

        return tiles


class TileCoder:
    '''
        All values should already be normalized from 0 to 1
    '''

    # class Tiling:
    #     def __init__(self, dimension=1, offset=0, divisions=None):
    #         self.dimension=dimension
    #         self.offset = offset
    #         self.divisions = divisions if divisions is not None else [10]
    #         self.full_length = 1
    #
    #         for idx, d in enumerate(divisions):
    #             self.full_length *= (d + 1)
    #
    #
    #     def tile_it(self, data):
    #         for d in data:

    def __init__(self, dimension=1, num_tilings=4, divisions=10, rng=np.random.RandomState()):
        # num_tilings - the number of tilings to create
        # resolutions - can be a single integer in which case all dimensions have the same resolution. Can be a list o
        #   of resolutions in which case the tilings may have different resolutions along different dimensions

        self.dimension = dimension
        self.num_tilings = num_tilings
        self.divisions = [divisions] * dimension if type(divisions) == int else divisions

        self.tiling_length = 1.0
        for div in self.divisions:
            self.tiling_length *= (div + 1)
        self.tiling_length = int(self.tiling_length)
        self.full_length = int(num_tilings * self.tiling_length)
        self.sparse_length = num_tilings

        self.rng = rng

        assert len(self.divisions) == dimension
        self.offsets = []
        self._init_offsets_()

    def _init_offsets_(self):
        self.offsets = []
        divisions = np.array(self.divisions)
        self.offsets.append(np.zeros(self.dimension))
        for idx in range(1, self.num_tilings):
            self.offsets.append(self.rng.random_sample(self.dimension) / divisions)

    def single_tiling(self, data, idx):
        offsets = self.offsets[idx]
        idx = 0
        multiplier = 1.0

        for d, div, offset in zip(data, self.divisions, offsets):
            assert 0.0 <= d <= 1.0
            idx += math.floor((d + offset) * div) * multiplier
            multiplier *= (div + 1)

        return int(idx)

    def tile_it(self, data):
        indices = []
        offset = 0
        for i in range(self.num_tilings):
            indices.append(self.single_tiling(data, i) + offset)
            offset += self.tiling_length

        return indices

    def exp_tile_it(self, data, buffer=None):
        sparse = self.tile_it(data)
        if buffer is not None:
            buffer[:] = 0.0
            buffer[sparse] = 1.0
        else:
            buffer = np.zeros(self.full_length)
            buffer[sparse] = 1.0

        return buffer

    def features(self, data):
        return self.exp_tile_it(data)

    # def _init_divisions(self):
    #     for idx in range(self.dimension):
    #         self.divisions.append(1.0 / self._get_resolution(idx))
    #
    # def _get_resolution(self, idx):
    #     if type(self.divisions) is list:
    #         return self.divisions[idx]
    #
    #     return self.divisions
    #
    # def _calculate_tiling_offset(self, dim_idx, tiling_idx):
    #     # supposedly evenly spaced offsets are a bad idea.
    #     return tiling_idx * self.divisions[dim_idx] / self.num_tilings
    #
    # def _calculate_tiled_index(self, val, dim_idx, tiling_idx):
    #     # idx - which dimension
    #     idx = int(float(val + self._calculate_tiling_offset(dim_idx, tiling_idx)) / self.divisions[dim_idx])
    #     return max(min(self._get_tiling_size_along_dimension(dim_idx) - 1, idx), 0)
    #
    # def _get_tiling_size_along_dimension(self, idx):
    #     return self._get_resolution(idx) + 1;
    #
    # def _calculate_tiling_size(self):
    #     tot = 0
    #     for idx in range(self.dimension):
    #         tot += self._get_tiling_size_along_dimension
    #     return tot
    #
    # def _create_tiling(self, vals, tiling_idx):
    #     ind = 0;
    #     pos = 0;
    #     offset_dimensions = 1
    #     for idx, val in enumerate(vals):
    #         div = self.divisions[idx]
    #         tile_index = self._calculate_tiled_index(val, idx, tiling_idx)
    #         pos += tile_index * offset_dimensions;
    #         offset_dimensions *= self._get_tiling_size_along_dimension(idx)
    #
    #     return pos
    #
    # def tile_indices(self, vals):
    #     assert len(vals) == self.dimension
    #     # if max(vals) > 1:
    #     # raise Warning("Val exceeds 1")
    #     # if min(vals) < 0:
    #     # raise Warning("Val is less than 0")
    #
    #     indices = []
    #     for tile_idx in range(self.num_tilings):
    #         indices.append(self._create_tiling(vals, tile_idx) + tile_idx * self.tiling_size)
    #
    #     return indices
    #
    # def tile_it(self, vals):
    #     return self.indices_to_tiles(self.tile_indices(vals))

    # def indices_to_tiles(self, indices):
    # return extended_indices_to_tiles(indices, self.all_tilings_size)
