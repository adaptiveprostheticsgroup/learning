import unittest
import numpy as np
from learning.leastsquares import *


"""
python -m unittest discover
"""

class TestLeastSquares(unittest.TestCase):
    
    def test_lstd_lambda(self):
        l = 0.3
        """
        1 0 0 0
        0 1 1 0
        1 0 0 0
        1 0 0 1
        """
        phi_m_indices = [[0], [1,2], [0], [0,3]]
        r = [1, 0.5, -1]
        weights = lstd_lambda(l, r, phi_m_indices, phi_length=4)
        
        self.assertTrue(np.allclose(weights, [-0.05128205, 0.05128205, 0.05128205, -0.15384615]))
        
if __name__=="__main__":
    
    suite = unittest.TestLoader().loadTestsFromTestCase(TestLeastSquares)
    unittest.TextTestRunner(verbosity=2).run(suite)