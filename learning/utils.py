from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from sklearn import linear_model
import numpy as np


def calc_return(target, gamma):
    # TODO: something is off with this one. It seems to be off by one timestep.
    target = list(target)
    target.reverse()
    g = 0.0
    gs = []
    for idx, r in enumerate(target):
        g = g * gamma + r
        gs.append(g)

    gs.reverse()
    return gs


def calc_vec_return(target, gamma):
    target = list(target)
    target.reverse()
    g = np.zeros(len(target[0]))
    gs = []
    for r in target:
        g = g * gamma + r
        gs.append(g)

    gs.reverse()
    return gs


def calc_ls(features, target, gamma):
    the_return = calc_return(target, gamma)
    reg = train_ls(features, the_return, gamma)
    return reg.predict(features)


def train_ls(features, target):
    reg = linear_model.LinearRegression(fit_intercept=False)
    reg.fit(features, target)
    return reg


def ts_to_gamma(ts):
    return 1 - 1.0 / ts


def gamma_to_ts(gamma):
    return np.rint(1.0 / (1 - gamma))
