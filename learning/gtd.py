import numpy as np


class GTD(object):
    """
        
    """

    def __init__(self, feature_length, gamma=0, alpha=0.1, alpha_h=0.01, l=1,
                 instantaneous=False, init_weight_val=0.0, start_indices=None):

        self.feature_length = feature_length
        self.weights = None
        self.h = None
        self.last_indices = None
        if init_weight_val:
            self.weights = np.full((feature_length), init_weight_val)
        else:
            self.weights = np.zeros((feature_length))

        self.h = np.zeros((feature_length))

        self.reset_for_new_episode(start_indices)

        self.gamma = gamma
        self.alpha = alpha
        self.alpha_h = alpha_h
        # lambda
        self.l = l
        self.instantaneous = instantaneous
        self.last_gamma = 0.0

    def predict(self, indices):
        return np.sum(self.weights[indices])

    def reset_for_new_episode(self, indices):
        self.last_indices = indices
        self.traces = np.zeros((self.feature_length))

    def clear_traces(self):
        self.traces = np.zeros((self.feature_length))

    @staticmethod
    def _calc_delta(beta, r, gamma, current_prediction, bootstrap_prediction):
        return beta * r + gamma * bootstrap_prediction - current_prediction

    @staticmethod
    def _update_traces(traces, last_gamma, _lambda, indices, rho):

        # e_t <- rho(e*last_gamma*lambda+phi)
        traces *= rho * last_gamma * _lambda
        traces[indices] += rho

    @staticmethod
    def _update_weights(weights, alpha, delta, gamma, _lambda, traces, indices, h):

        # theta <-theta + alpha*(delta*traces-gamma(1-lambda)*phi_{t+1}*traces'h)

        weights += alpha * delta * traces
        weights[indices] -= alpha * gamma * (1 - _lambda) * np.inner(traces, h)

    @staticmethod
    def _update_h(h, alpha, delta, traces, last_indices):
        # h<-h+alpha_h(delta*e - (h'phi_t)*phi_t)
        h[last_indices] -= alpha * np.sum(h[last_indices])
        h += alpha * delta * traces

    @staticmethod
    def _calc_beta(gamma, instantaneous):
        beta = 1.0
        if instantaneous == True:
            beta = 1.0 - gamma

        return beta

    def update(self, r, feature_indices, rho, gamma=None, alpha=None, alpha_h=None):

        feature_indices = list(feature_indices)

        delta = 0.0

        if self.last_indices is not None:
            _gamma = gamma if gamma is not None else self.gamma
            _alpha = alpha if alpha is not None else self.alpha
            _alpha_h = alpha_h if alpha_h is not None else self.alpha_h

            # \hat{v}<-theta^T Phi
            current_prediction = self.predict(self.last_indices)
            # \hat{v}'<-theta^T Phi'
            bootstrap_prediction = self.predict(feature_indices)

            beta = 1.0

            beta = self._calc_beta(_gamma, self.instantaneous)

            delta = self._calc_delta(beta, r, _gamma,
                                     current_prediction=current_prediction,
                                     bootstrap_prediction=bootstrap_prediction)

            self._update_traces(traces=self.traces, last_gamma=self.last_gamma, _lambda=self.l,
                                indices=self.last_indices, rho=rho)
            self._update_weights(weights=self.weights, alpha=_alpha, delta=delta, gamma=_gamma, _lambda=self.l,
                                 traces=self.traces, indices=feature_indices, h=self.h)

            self._update_h(self.h, alpha=_alpha_h, delta=delta, traces=self.traces, last_indices=self.last_indices)

            self.last_gamma = _gamma

        self.last_indices = feature_indices

        return delta
