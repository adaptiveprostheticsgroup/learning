from learning.true_online_td import TrueOnlineTD
import numpy as np

class VarToGo(object):
    def __init__(self, feature_length, gamma=0, alpha=0.1, l=1, 
                instantaneous=False, init_weight_val=0.0, start_indices=None):
        self.feature_length = feature_length
        self.weights = None
        self.last_indices = None
        if init_weight_val:
            self.weights = np.full((feature_length), init_weight_val)
        else:
            self.weights = np.zeros((feature_length)) 
        
        self.reset_for_new_episode(start_indices)
            
        self.gamma = gamma
        self.alpha = alpha
        #lambda
        self.l = l
        self.instantaneous = instantaneous        
        
    @staticmethod
    def _calc_delta(beta, r, gamma, primary_bootstrap_prediction, current_prediction, bootstrap_prediction):
        #delta = z^2 + 2*r*gamma*primary_bootstrap_prediction + gamma*bootstrap_prediction - current_prediction
        cumulant = beta * r
        
        return (beta*r)**2 + 2*r*beta*gamma*primary_bootstrap_prediction + gamma*bootstrap_prediction - current_prediction
    
    def predict(self, indices):
        return np.sum(self.weights[indices])
    
    def reset_for_new_episode(self, indices):
        self.last_indices = indices
        self.last_value = 0.0
        self.traces = np.zeros((self.feature_length))
        
    def clear_traces(self):
        self.traces = np.zeros((self.feature_length))
        
    @staticmethod
    def _update_traces(traces, gamma, _lambda, indices, alpha):
        TrueOnlineTD._update_traces(traces, gamma, _lambda, indices, alpha)
        
    @staticmethod
    def _update_weights(weights, alpha, delta, current_prediction, last_prediction, traces, last_indices):        
        TrueOnlineTD._update_weights(weights, alpha, delta, current_prediction, last_prediction, traces, last_indices)
    
    def update(self, r, feature_indices, primary_bootstrap_prediction, gamma=None, alpha=None):
        feature_indices = list(feature_indices)
        
        delta = 0.0
        
        if self.last_indices:
            _gamma = gamma if gamma is not None else self.gamma
            _alpha = alpha if alpha is not None else self.alpha
                    
            # \hat{v}<-theta^T Phi
            current_prediction = self.predict(self.last_indices)
            # \hat{v}'<-theta^T Phi'
            bootstrap_prediction = self.predict(feature_indices)
            
            beta = 1.0
            
            beta = TrueOnlineTD._calc_beta(_gamma, self.instantaneous)
            
            delta = self._calc_delta(beta, r, _gamma, primary_bootstrap_prediction=primary_bootstrap_prediction,
                                     current_prediction=current_prediction,
                                     bootstrap_prediction=bootstrap_prediction)
            
            self._update_traces(traces=self.traces, gamma=_gamma, _lambda=self.l, indices=self.last_indices, alpha=_alpha)
            self._update_weights(weights=self.weights, alpha=_alpha, delta=delta, current_prediction=current_prediction, 
                                last_prediction=self.last_value, 
                                traces=self.traces, 
                                last_indices=self.last_indices)
            
            
            self.last_value = bootstrap_prediction
        self.last_indices = feature_indices
        
        return delta        
        