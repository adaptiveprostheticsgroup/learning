#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np


class GVF(object):
    """
        GVF is a base class for the various TD algorithms. It handles feature vectors that are
        non-sparse, sparse, and sparse binary. However, once you pick one do not switch.
        
        Note, in hindsight I think that implementing the TD algorithms in this way (using inheritance) was
        a design mistake. Although, there is a significant amount of the same code between them, doing this has
        made the structure somewhat rigid to work with. Additionally, it is now not as nicely layed out for
        someone trying to just look at a particular algorithm to see what's going on. 
        
        Additionally, implementing the 3 different feature types, non-sparse, sparse, and sparse binary, via specifying
        a combination of parameters is a bit awkward. I may have been better off to just duplicate some code
        and make the calls very explicit.
        
        That being said, I'm not keen to make changes at this point.
    
        feature_length(int) - the length of the weight vector/feature vector.
        gamma(float) - Gamma to use, discounting factor, how far in the future we look, smaller = look closer.
        alpha(float) - learning rate. This can be overridden on a per-timestep basis. Presently, does not
            normalize. So you need to scale alpha yourself ie. 0.1/#active features            
        l(float) - lambda, how much bootstrapping to do. l=1 is equal to Monte Carlo, l=0 is full bootstrapping
        instantaneous(bool) - If you set this true then the target value 'r' will be prescaled in update by (1-gamma)
    """

    def __init__(self, feature_length, gamma=0, alpha=0.1, l=1, instantaneous=False, init_weights=False):

        self.feature_length = feature_length

        # I think it is easiest in numpy to work with row vectors
        self.weights = None

        if init_weights:
            if isinstance(init_weights, float):
                self.weights = np.full((feature_length), init_weights)
            elif isinstance(init_weights, dict):
                self.weights = np.random.sample((feature_length)) * init_weights["scale"] + init_weights["offset"]

        if self.weights is None:
            self.weights = np.zeros((feature_length))

        self.clear_traces()
        self.init_last_features()

        self.gamma = gamma
        self.alpha = alpha

        # lambda
        self.l = l
        self.instantaneous = instantaneous

        # True when learning is enabled. 
        self.learning = True

        self.previous_gamma = 0.0

    def update(self, r, features=None, feature_indices=None, gamma=None, alpha=None):
        """
        Call to make a learning update.
        
        There are 3 steps in a TD algorithm:
            - calculate delta, the TD error
            - update traces
            - update weights
            
        features(list): If using non-sparse, or sparse non-binary, set this vector
        feature_indices(list): use for sparse representations. If present, we assume representation is sparse. If features is None, then
            we assume sparse binary
        gamma(float) - Time dependent gamma. Usually gamma is fixed, but this allows you to override the gamma on a per timestep basis,
            say at the end of an episode you will want to set gamma = 0
        alpha(float) - Learning rate, usually fixed, but can be overridden on a per timestep basis, say if you are using auto-step alphas,
            or the number of active features is changing in your representation
        """
        delta = 0

        # Only update if learning is enabled
        if self.learning:
            # override gamma and alpha if set
            gamma = gamma if gamma != None else self.gamma
            alpha = alpha if alpha != None else self.alpha

            # Calc delta
            delta = self._calculate_delta(new_features=features,
                                          indices=feature_indices, gamma=gamma, r=r)

            self._calculate_traces(gamma=self.previous_gamma, alpha=alpha)

            self._calculate_weights(delta=delta, alpha=alpha)

            # need to keep track of features and indices
            # important that we take a copy, don't just set it equal
            self.last_features = None if features is None else features.copy()
            self.last_features_indices = None if feature_indices is None else list(feature_indices)

        return delta

    def set_learning(self, learning):
        self.learning = learning

    def clear_traces(self):
        # cut traces
        self.traces = np.zeros((self.feature_length))

    def init_last_features(self):
        self.last_features = np.zeros((self.feature_length))
        self.last_features_indices = None

    def reset_for_new_episode(self):
        """
        In the episodic case, when a new episode starts we need to cut our traces and reset various things
        """
        self.clear_traces()
        self.init_last_features()
        self.set_weights(self.weights)

    @staticmethod
    def _predict(weights, features, indices):
        """
         Makes our actual prediction for the given state
         
         weights(numpy.ndarray) - learned weight vector
         features(list) - Specify if non-sparse, or sparse non-binary
         indices(list) - If not None, then this is a sparse vector. If features is also None, then we are doing sparse binary
        """
        # The prediction
        p = 0.0

        # Not sparse
        if indices is None:
            if features is None:
                return p
            if len(features) != len(weights):
                raise Exception

            # Assume we are doing non-sparse calculations
            p = np.inner(weights, features)

        # sparse
        else:
            if features is None:
                # Assuming Binary features
                p = np.sum(weights[indices])
            else:
                # Non-binary
                p = np.inner(weights[indices], features)

        return p

    def predict(self, features=None, indices=None):
        """
        Publicly accesible predict call
        
         features(list) - Specify if non-sparse, or sparse non-binary
         indices(list) - If not None, then this is a sparse vector. If features is also None, then we are doing sparse binary
        """
        return self._predict(self.weights, features, indices)

    def _calculate_traces(self, gamma, alpha):
        """
        Calculate the traces
        """
        raise NotImplemented

    def _calculate_delta(self, new_features, indices, gamma, r):
        """
        Calculate TD error, delta
        """
        raise NotImplemented

    def _calculate_weights(self, delta, alpha, current_features,
                           current_features_indices):
        """
        Calculate new weights
        """
        raise NotImplemented

    def set_weights(self, weights):
        self.weights = weights.copy()

    @staticmethod
    def calculate_gamma(frequency, time):
        """
        Calculates gamma given a certain update frequency, and desired lookahead time
        
        frequency(float) - update frequency
        time(float) - number of seconds lookahead desired
        """
        return 1 - (1.0 / (frequency * time))

    @staticmethod
    def calculate_beta(gamma, instantaneous):
        """
        This is used for calculation of delta.
        
        gamma(float) - discounting rate
        instantaneous(bool) - If true then the reward in the update will be prescaled (1-gamma).
        """
        return (1.0 - gamma) if instantaneous else 1.0

    @staticmethod
    def calculate_gamma_from_time_steps(steps):
        """
        Calculates gamma given a number of timesteps lookahead desired
        """
        return 1 - 1.0 / steps

    @staticmethod
    def calculate_time_steps_from_gamma(gamma):
        """
        Given a gamma, how many timesteps does this "lookahead". This is a rough estimate.
        
        gamma(float) - discounting rate
        """
        return 1 / (1 - gamma)


class TrueOnlineTD(GVF):
    """
    Implementation of True Online TD 
    
    Van Siejen, H. & Sutton, R.S., 2014. True Online TD ( λ ). In Proceedings of the 31st International Conference on Machine Learning. Beijing, China, pp. 692–700.

    True Online TD is the first TD algorithm to reconcile the backward and forward views. It also does away with the need to choose a type of traces (accumulating or replacing).
    
    Note that this was implemented using the algorithm as listed in Harm's paper (Eqns. 10-12). However, there is a soon to be published reworking of this algorithm that is cleaner in
    the sense that it uses the same TD error calculation as most of us are used to.
    
    """

    def __init__(self, feature_length, gamma=0, alpha=0.1, l=1,
                 instantaneous=False, init_weights=False):
        super(TrueOnlineTD, self).__init__(feature_length=feature_length, gamma=gamma, alpha=alpha, l=l,
                                           instantaneous=instantaneous, init_weights=init_weights)
        self.previous_weights = np.zeros((feature_length))

    def _calculate_delta(self, new_features, indices, gamma, r):
        return self._calc_delta(current_weights=self.weights,
                                previous_weights=self.previous_weights,
                                previous_features=self.last_features,
                                previous_features_indices=self.last_features_indices,
                                new_features=new_features, new_features_indices=indices, gamma=gamma,
                                r=r, instantaneous=self.instantaneous)

    @staticmethod
    def _calc_delta(current_weights, previous_weights, previous_features, previous_features_indices, new_features,
                    new_features_indices, gamma, r, instantaneous):
        """
        Eq. 10 \delta=\beta R_{t+1}+\gamma \theta_t^T\phi_{t+1} - \theta_{t-1}^T\phi_t
        """

        beta = TrueOnlineTD.calculate_beta(gamma, instantaneous)
        return beta * r + gamma * TrueOnlineTD._predict(weights=current_weights, features=new_features,
                                                        indices=new_features_indices) - \
               TrueOnlineTD._predict(weights=previous_weights, features=previous_features,
                                     indices=previous_features_indices)

    def _calculate_traces(self, gamma, alpha):
        self._calc_traces(self.traces, self.last_features, self.last_features_indices, self.l, gamma, alpha)

    @staticmethod
    def _calc_traces(traces, previous_features, previous_features_indices, l, gamma, alpha):
        """
        Eq 11. e_t=\gamma\lambda e_{t-1} + \alpha_t\phi_t-\alpha_t\gamma\lambda[e_{t-1}^T\phi_t]\phi_t
        
        traces_copy(numpy.ndarray) is used to prevent allocating memory every time this is called. It should be the same size as traces
        """

        # TODO: Error, the gamma should come from the previous timestep
        l_gamma = l * gamma
        # sparse
        if previous_features_indices is not None:
            # non-binary
            if previous_features is not None:
                ep = np.inner(traces[previous_features_indices], previous_features)
                traces *= l_gamma
                traces[previous_features_indices] += alpha * (1 - l_gamma * ep) * previous_features
            # binary
            else:
                ep = np.sum(traces[previous_features_indices])
                traces *= l_gamma
                traces[previous_features_indices] += alpha * (1 - l_gamma * ep)
        # non-sparse
        else:
            np.copyto(traces, l_gamma * traces + \
                      alpha * previous_features * (1 - l_gamma * (np.inner(traces, previous_features))))

    def _calculate_weights(self, delta, alpha):
        self._calc_weights(delta=delta, traces=self.traces, alpha=alpha,
                           previous_features=self.last_features,
                           previous_features_indices=self.last_features_indices,
                           previous_weights=self.previous_weights, current_weights=self.weights)

    @staticmethod
    def _calc_weights(delta, traces, alpha, previous_features,
                      previous_features_indices, previous_weights, current_weights):
        """
        Eq 12. \theta_{t+1} = \theta_t + \delta_t e_t + \alpha_t[\theta_{t-1}^T\phi_t-\theta_t^T\phi_t]\phi_t
        """

        # sparse
        if previous_features_indices is not None:
            diff = TrueOnlineTD._predict(
                previous_weights, previous_features,
                previous_features_indices) - TrueOnlineTD._predict(current_weights,
                                                                   previous_features, previous_features_indices)

            if previous_features is not None:
                current_weights[previous_features_indices] += alpha * diff * previous_features
            # binary
            else:
                current_weights[previous_features_indices] += alpha * diff

            current_weights += delta * traces

        # non-sparse
        else:
            current_weights += delta * traces + \
                               alpha * (np.inner(previous_weights,
                                                 previous_features) - np.inner(current_weights,
                                                                               previous_features)) * previous_features

    def update(self, r, features=None, feature_indices=None, gamma=None,
               alpha=None):
        delta = GVF.update(self, r, features, feature_indices, gamma, alpha)
        # need to keep a copy of the weights
        np.copyto(self.previous_weights, self.weights)
        return delta

    def set_weights(self, weights):
        """
        Want both weights and previous_weights to be set to the same thing so that the first call to update will produce 0 change
        """
        np.copyto(self.weights, weights)
        np.copyto(self.previous_weights, weights)


class TD(GVF):

    def _calculate_delta(self, new_features, indices, gamma, r):
        return self._calc_delta(weights=self.weights,
                                previous_features=self.last_features,
                                previous_features_indices=self.last_features_indices,
                                new_features=new_features, new_features_indices=indices, gamma=gamma,
                                r=r, instantaneous=self.instantaneous)

    @classmethod
    def _calc_delta(cls, weights, previous_features,
                    previous_features_indices, new_features, new_features_indices, gamma, r,
                    instantaneous=False):
        beta = cls.calculate_beta(gamma, instantaneous)
        return beta * r + gamma * cls._predict(weights, new_features,
                                               new_features_indices) - cls._predict(weights, previous_features,
                                                                                    previous_features_indices)

    def _calculate_weights(self, delta, alpha=None):
        self._calc_weights(delta=delta, traces=self.traces,
                           current_weights=self.weights)

    @staticmethod
    def _calc_weights(delta, traces, current_weights):
        current_weights += delta * traces


class AccumulatingTD(TD):

    def _calculate_traces(self, gamma, alpha):
        self._calc_traces(traces=self.traces,
                          previous_features=self.last_features,
                          previous_features_indices=self.last_features_indices, l=self.l,
                          gamma=gamma, alpha=alpha)

    @staticmethod
    def _calc_traces(traces, previous_features, previous_features_indices, l, gamma, alpha):
        l_gamma = l * gamma
        if previous_features_indices is not None:
            traces *= l_gamma
            if previous_features is not None:
                traces[previous_features_indices] += alpha * previous_features
            else:
                traces[previous_features_indices] += alpha
        else:
            traces *= l_gamma
            traces += alpha * previous_features

# class ReplacingTD(TD):
## TODO: Unit Test
# def _calculate_traces(self, gamma, alpha):
# self._calc_traces(traces=self.traces,
# previous_features=self.last_features,
# previous_features_indices=self.last_features_indices, l=self.l,
# gamma=gamma, alpha=alpha)

# @staticmethod
# def _calc_traces(traces, previous_features, previous_features_indices, l, gamma, alpha):
# l_gamma = l*gamma
# if previous_features_indices is not None:
# traces*=l_gamma
# if previous_features is not None:
# traces[previous_features_indices] += alpha*previous_features
# else:
# traces[previous_features_indices] += alpha
# else:
# traces*=l_gamma*traces
# traces+=alpha*previous_features

# np.clip(traces, 0, 1, traces)

# class GVFGroup:
# def __init__(self):
# self.gvfs = []
# def process(self, f, s):
# targets = []
# predictions = []
# gammas = []
# target_multiplier = []
# details = []
# for gvf in self.gvfs:
# weights = gvf.weights
# targets.append(s)
# predictions.append(gvf.predict(f))
# gvf.update(f, s)
# details.append(Details(weights = weights.tolist(),
# features=f.tolist(), alpha=gvf.alpha,
# l=gvf.l, traces = gvf.traces.tolist()))
# gammas.append(gvf.gamma)
# target_multiplier.append(1 if not gvf.use_beta else 1-gvf.gamma)

# return {'targets': targets, 'predictions': predictions, 'gammas': gammas, 'target_multiplier': target_multiplier, 'details': details}

# def add_gvf(self, gvf):
# self.gvfs.append(gvf)
