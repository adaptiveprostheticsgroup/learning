import unittest
import numpy as np
from learning.gvf import *


"""
python -m unittest discover
"""

class TestGVF(unittest.TestCase):
    
    def get_default_gvf(self):
        return GVF(feature_length=4, gamma=0.5, alpha=0.1, l=0.5)
        
    @staticmethod
    def populate_gvf_full(gvf):
        gvf.weights = TestGVF.get_default_weights()
        gvf.traces = TestGVF.get_default_traces()
        gvf.last_features = TestGVF.get_default_last_features_full()

    @staticmethod
    def populate_gvf_sparse(gvf):
        gvf.weights = TestGVF.get_default_weights()
        gvf.traces = TestGVF.get_default_traces()
        (gvf.last_features, gvf.last_features_indices) = TestGVF.get_default_last_features_sparse()
        
    @staticmethod
    def populate_gvf_sparse_binary(gvf):
        gvf.weights = TestGVF.get_default_weights()
        gvf.traces = TestGVF.get_default_traces()
        (gvf.last_features, gvf.last_features_indices) =\
            TestGVF.get_default_last_features_sparse_binary()
        
    @staticmethod
    def get_default_weights():
        return np.array([0.1, 0.2,-1,3])

    @staticmethod
    def get_default_traces():
        return np.array([0.1, 0, 0.4, 0.9])
    
    @staticmethod
    def get_default_last_features_full():
        return np.array([-2, 2, 0, 0.1])
    
    @staticmethod
    def get_default_last_features_sparse():
        return (np.array([-2,2,0.1]), np.array([0,1,3]))
    
    @staticmethod
    def get_default_last_features_sparse_binary():
        return (None, np.array([1,3]))
    
    def test_predict_full(self):
        gvf = self.get_default_gvf()
        TestGVF.populate_gvf_full(gvf)
        prediction = gvf.predict(features=np.ones((4)))
        
        self.assertAlmostEqual(prediction, 2.3)
        
    def test_predict_sparse(self):
        gvf = self.get_default_gvf()
        TestGVF.populate_gvf_sparse(gvf)
        prediction = gvf.predict(features=np.array([0.1,0.5]), indices=[0,3])
        
        self.assertAlmostEqual(prediction, 1.51)
        
    def test_predict_sparse_binary(self):
        gvf = self.get_default_gvf()
        TestGVF.populate_gvf_sparse_binary(gvf)
        prediction = gvf.predict(indices=[0,3])
        
        self.assertAlmostEqual(prediction, 3.1)
        
    def test_calculate_beta(self):
        self.assertAlmostEqual(GVF.calculate_beta(0.5, False), 1.0)
        self.assertAlmostEqual(GVF.calculate_beta(0.5, True), 0.5)
        
    def test_calculate_gamma(self):
        self.assertAlmostEqual(GVF.calculate_gamma(10, 8), 0.9875)
        self.assertAlmostEqual(GVF.calculate_gamma(10, 2), 0.95)
        self.assertAlmostEqual(GVF.calculate_gamma(10, 0.5), 0.8)
        self.assertAlmostEqual(GVF.calculate_gamma(10, 0.1), 0)
        
class TestAccumulatingTD(unittest.TestCase):
    
    def _get_default_gvf(self):
        gvf = AccumulatingTD(feature_length=4, gamma=0.5, alpha=0.1, l=0.5)
        gvf.weights = np.array([0.1, 0.2,-1,3])
        gvf.traces = np.array([0.1, 0, 0.4, 0.9])
        gvf.last_features = np.array([-2, 2, 0, 0.1])
        return gvf
    
    def test_predict(self):
        gvf = self._get_default_gvf()
        prediction = GVF._predict(gvf.weights, np.ones((4)), indices=None)
        
        self.assertAlmostEqual(prediction, 2.3)
        
    def test_calculate_traces(self):
        gvf = self._get_default_gvf()
        gvf._calculate_traces(gvf.gamma, gvf.alpha)
        self.assertTrue(np.allclose(gvf.traces, np.array([-0.175, 0.2, 0.1, 0.235])))
    
    def test_calculate_delta(self):
        gvf = self._get_default_gvf()
        delta = gvf._calculate_delta(new_features=np.array([10, -4, 0, 1]),
                                     indices=None, gamma=gvf.gamma, r=1)
        
        self.assertAlmostEqual(2.1, delta)
        
    def test_calculate_weights(self):
        gvf = self._get_default_gvf()
        gvf._calculate_weights(delta=0.5)
        self.assertTrue(np.allclose(np.array([0.15, 0.2, -0.8, 3.45]), gvf.weights))

class TestTrueOnlineTD(unittest.TestCase):
    
    def get_default_gvf_full(self):
        gvf = TrueOnlineTD(feature_length=4, gamma=0.5, alpha=0.1, l=0.5)
        TestGVF.populate_gvf_full(gvf)
        gvf.previous_weights = TestTrueOnlineTD.get_default_previous_weights()
        return gvf
    
    def get_default_gvf_sparse(self):
        gvf = TrueOnlineTD(feature_length=4, gamma=0.5, alpha=0.1, l=0.5)
        TestGVF.populate_gvf_sparse(gvf)
        gvf.previous_weights = TestTrueOnlineTD.get_default_previous_weights()
        return gvf
    
    def get_default_gvf_sparse_binary(self):
        gvf = TrueOnlineTD(feature_length=4, gamma=0.5, alpha=0.1, l=0.5)
        TestGVF.populate_gvf_sparse_binary(gvf)
        gvf.previous_weights = TestTrueOnlineTD.get_default_previous_weights()
        return gvf    
    
    @staticmethod
    def get_default_previous_weights():
        return np.array([0.0, -0.1, 0.5, 1])
        
    def test_calculate_delta_full(self):
        gvf = self.get_default_gvf_full()
        delta = gvf._calculate_delta(new_features=np.array([10, -4, 0, 1]), indices=None, gamma=gvf.gamma, r=1)
        
        self.assertAlmostEqual(2.7, delta)
        
    def test_calculate_delta_sparse(self):
        gvf = self.get_default_gvf_sparse()
        delta = gvf._calculate_delta(new_features=np.array([10, -4, 1]), indices=[0,1,3], gamma=gvf.gamma, r=1)
        
        self.assertAlmostEqual(2.7, delta)        
        
    def test_calculate_delta_sparse_binary(self):
        gvf = self.get_default_gvf_sparse_binary()
        delta = gvf._calculate_delta(new_features=None, indices=[0,1], gamma=gvf.gamma, r=1)
        
        self.assertAlmostEqual(0.25, delta)        
        
    def test_calculate_traces_full(self):
        gvf = self.get_default_gvf_full()
        TrueOnlineTD._calc_traces(traces=gvf.traces, previous_features=gvf.last_features, previous_features_indices=None, l=gvf.l, gamma=gvf.gamma, alpha=gvf.alpha)
        self.assertTrue(np.allclose(gvf.traces, np.array([-0.1805, 0.2055, 0.1, 0.235275])))
        
    def test_calculate_traces_sparse(self):
        gvf = self.get_default_gvf_sparse()
        TrueOnlineTD._calc_traces(traces=gvf.traces,
                                           previous_features=gvf.last_features,
                                           previous_features_indices=gvf.last_features_indices, l=gvf.l,
                                           gamma=gvf.gamma, alpha=gvf.alpha)
        self.assertTrue(np.allclose(gvf.traces, np.array([-0.1805, 0.2055, 0.1, 0.235275])))
        
    def test_calculate_traces_sparse_binary(self):
        gvf = self.get_default_gvf_sparse_binary()
        TrueOnlineTD._calc_traces(traces=gvf.traces, previous_features=None,
                                  previous_features_indices=gvf.last_features_indices, l=gvf.l,
                                  gamma=gvf.gamma, alpha=gvf.alpha)
        self.assertTrue(np.allclose(gvf.traces, np.array([0.025, 0.0775, 0.1, 0.3025])))
        
    def test_calculate_weights_full(self):
        gvf = self.get_default_gvf_full()
        TrueOnlineTD._calc_weights(delta=0.1, traces=gvf.traces,
                                             alpha=gvf.alpha, previous_features=gvf.last_features,
                                             previous_features_indices=None,
                                             previous_weights=gvf.previous_weights, current_weights=gvf.weights)
        self.assertTrue(np.allclose(np.array([0.23, 0.08, -0.96, 3.084]), gvf.weights))
        
    def test_calculate_weights_sparse(self):
        gvf = self.get_default_gvf_sparse()
        TrueOnlineTD._calc_weights(delta=0.1, traces=gvf.traces,
                                             alpha=gvf.alpha, previous_features=gvf.last_features,
                                             previous_features_indices=gvf.last_features_indices,
                                             previous_weights=gvf.previous_weights, current_weights=gvf.weights)
        self.assertTrue(np.allclose(np.array([0.23, 0.08, -0.96, 3.084]), gvf.weights))
        
    def test_calculate_weights_sparse_binary(self):
        gvf = self.get_default_gvf_sparse_binary()
        TrueOnlineTD._calc_weights(delta=0.1, traces=gvf.traces,
                                             alpha=gvf.alpha, previous_features=gvf.last_features,
                                             previous_features_indices=gvf.last_features_indices,
                                             previous_weights=gvf.previous_weights, current_weights=gvf.weights)
        self.assertTrue(np.allclose(np.array([0.11, -0.03, -0.96, 2.86]), gvf.weights))
        
    def test_update_full(self):
        gvf = self.get_default_gvf_full()
        gvf.update(r=1, features=np.array([10, -4, 0, 1]))
        self.assertTrue(np.allclose(gvf.traces, np.array([-0.1805, 0.2055, 0.1, 0.235275])))
        self.assertTrue(np.allclose(gvf.weights, np.array([-0.26735, 0.63485, -0.73, 3.6292425])))
        
        self.assertAlmostEqual(gvf.predict(features=np.array([1,1,1,1])), np.sum([-0.26735, 0.63485, -0.73, 3.6292425]))
        
    def test_update_sparse(self):
        gvf = self.get_default_gvf_sparse()
        gvf.update(r=1, features=np.array([10, -4, 1]), feature_indices=[0,1,3])
        self.assertTrue(np.allclose(gvf.traces, np.array([-0.1805, 0.2055, 0.1, 0.235275])))
        self.assertTrue(np.allclose(gvf.weights, np.array([-0.26735, 0.63485, -0.73, 3.6292425])))
        
        self.assertAlmostEqual(gvf.predict(features=np.array([1,1,1,1])), np.sum([-0.26735, 0.63485, -0.73, 3.6292425]))
        
    def test_update_sparse_binary(self):
        gvf = self.get_default_gvf_sparse_binary()
        gvf.update(r=1, feature_indices=[0,1])
        self.assertTrue(np.allclose(gvf.traces, np.array([0.025, 0.0775, 0.1, 0.3025])))
        self.assertTrue(np.allclose(gvf.weights, np.array([0.10625, -0.010625, -0.975, 2.845625])))
        
        self.assertAlmostEqual(gvf.predict(features=np.array([1,1,1,1])), np.sum([0.10625, -0.010625, -0.975, 2.845625])) 
        
        
class TestTrueOnlineTD2(unittest.TestCase):
    def test_update(self):
        
        gvf = TrueOnlineTD2(feature_length=4, gamma=0.5, alpha=0.1, l=0.5)
        TestGVF.populate_gvf_sparse_binary(gvf)
        gvf.update(r=1, feature_indices=[0,1])
        #self.assertTrue(np.allclose(gvf.traces, np.array([0.025, 0.0775, 0.1, 0.3025])))
        #self.assertTrue(np.allclose(gvf.weights, np.array([0.10625, -0.010625, -0.975, 2.845625])))
        
        self.assertAlmostEqual(gvf.predict(features=np.array([1,1,1,1])), np.sum([0.10625, -0.010625, -0.975, 2.845625])) 

if __name__=="__main__":
    
    suite = unittest.TestLoader().loadTestsFromTestCase(TestGVF)
    unittest.TextTestRunner(verbosity=2).run(suite)
    
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAccumulatingTD)
    unittest.TextTestRunner(verbosity=2).run(suite)    
    
    suite = unittest.TestLoader().loadTestsFromTestCase(TestTrueOnlineTD)
    unittest.TextTestRunner(verbosity=2).run(suite)
    
    #suite = unittest.TestLoader().loadTestsFromTestCase(TestTrueOnlineTD2)
    #unittest.TextTestRunner(verbosity=2).run(suite)