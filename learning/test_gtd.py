import unittest
import numpy as np
from learning.gtd import GTD


"""
python -m unittest discover
"""

class TestGTD(unittest.TestCase):

    def test_calc_delta(self):
        delta = GTD._calc_delta(beta=0.25, r=0.2, gamma=0.75, current_prediction=2.00, bootstrap_prediction=3.0)
        
        self.assertAlmostEqual(0.3, delta)
        
    
    def test_calc_traces(self):
        traces = np.array([0.1, 0.2])
        GTD._update_traces(traces, last_gamma=0.1, _lambda=0.6, indices=np.array([1]), rho=0.1)
        
        self.assertTrue(np.allclose([0.0006, 0.1012], traces))
        
    def test_update_weights(self):
        weights = np.array([1.0, 2.0])
        traces = np.array([0.0006, 0.1012])
        indices = np.array([0, 1])
        h = np.array([0.3, -0.1])
        
        GTD._update_weights(weights, alpha=0.1, delta=0.3, 
                                    gamma=0.75,
                                    _lambda=0.6,
                                    traces=traces, 
                                    indices=indices,
                                    h=h)
        
        self.assertTrue(np.allclose([1.0003162, 2.0033342], weights))
        
    def test_update_h(self):
        traces = np.array([0.0006, 0.1012])
        h = np.array([0.3, -0.1])
        last_indices = np.array([1])
        
        GTD._update_h(h, alpha=0.01, delta=0.3, traces=traces, last_indices=last_indices)
        
        self.assertTrue(np.allclose([0.3000018, -0.0986964], h))
        
        
    def test_update(self):
        weights = np.array([1.0, 2.0])
        traces = np.array([0.1, 0.2])
        indices = np.array([0, 1])
        last_indices = np.array([1])
        h = np.array([0.3, -0.1])
        
        gvf = GTD(2, gamma=0.75, l=0.6, alpha=0.1, alpha_h=0.01, instantaneous=True, start_indices=last_indices)
        gvf.weights = weights
        gvf.h = h
        gvf.traces = traces
        gvf.last_gamma = 0.1
        
        gvf.update(0.2, feature_indices=indices, rho=0.1)
        
        self.assertTrue(np.allclose([0.0006, 0.1012], traces))
        self.assertTrue(np.allclose(gvf.last_indices, indices))
        self.assertTrue(np.allclose([1.0003162, 2.0033342], gvf.weights))
        self.assertTrue(np.allclose([0.3000018, -0.0986964], h))
        
        
if __name__=="__main__":
    
    suite = unittest.TestLoader().loadTestsFromTestCase(TestGTD)
    unittest.TextTestRunner(verbosity=2).run(suite)