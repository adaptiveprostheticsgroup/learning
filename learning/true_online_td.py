import numpy as np

class TrueOnlineTD(object):
    """
        Using Harm's rephrasing of the algorithm. Just supporting Binary features right now
        
        van Seijen, H., Mahmood, A. R., Pilarski, P. M., Machado, M. C., & Sutton, R. S. (2015). 
        True Online Temporal-Difference Learning, (DECEMBER). Retrieved from http://arxiv.org/abs/1512.04087
        
    """
    def __init__(self, feature_length, gamma=0, alpha=0.1, l=1, 
                instantaneous=False, init_weight_val=0.0, start_indices=None):
        
        self.feature_length = feature_length
        self.weights = None
        self.last_indices = None
        if init_weight_val:
            self.weights = np.full((feature_length), init_weight_val)
        else:
            self.weights = np.zeros((feature_length)) 
        
        self.reset_for_new_episode(start_indices)
            
        self.gamma = gamma
        self.alpha = alpha
        #lambda
        self.l = l
        self.instantaneous = instantaneous
        
    def predict(self, indices):
        return np.sum(self.weights[indices])
    
    def reset_for_new_episode(self, indices):
        self.last_indices = indices
        self.last_value = 0.0
        self.traces = np.zeros((self.feature_length))
        
    def clear_traces(self):
        self.traces = np.zeros((self.feature_length))
        
    @staticmethod
    def _calc_delta(beta, r, gamma, current_prediction, bootstrap_prediction):
        return beta*r + gamma*bootstrap_prediction-current_prediction
    
    @staticmethod
    def _update_traces(traces, gamma, _lambda, indices, alpha):

        # e<-gamma*lambda*e + phi- alpha*gamma*lambda(e'phi)phi
        # e<-gamma*lambda*e + phi(1-alpha*gamma*lambda(e'phi))     
        
        traces_inner = np.sum(traces[indices])
        
        traces*=gamma*_lambda
        
        traces[indices] += (1.0 - alpha*gamma*_lambda*traces_inner)
        
    @staticmethod
    def _update_weights(weights, alpha, delta, current_prediction, last_prediction, traces, last_indices):
        
        # theta <-theta + alpha*(delta + current_prediction - last_prediction)e - alpha(current_prediction - last_prediction)phi
        
        v_delta = current_prediction - last_prediction
        
        weights += alpha*(delta + v_delta)*traces
        
        weights[last_indices] -= alpha*v_delta
        
    @staticmethod
    def _calc_beta(gamma, instantaneous):
        beta = 1.0
        if instantaneous == True:
            beta = 1.0 - gamma
            
        return beta
            
        
    def update(self, r, feature_indices, gamma=None, alpha=None):
        
        feature_indices = list(feature_indices)
        
        delta = 0.0
        
        if self.last_indices:
            _gamma = gamma if gamma is not None else self.gamma
            _alpha = alpha if alpha is not None else self.alpha
                    
            # \hat{v}<-theta^T Phi
            current_prediction = self.predict(self.last_indices)
            # \hat{v}'<-theta^T Phi'
            bootstrap_prediction = self.predict(feature_indices)
            
            beta = 1.0
            
            beta = self._calc_beta(_gamma, self.instantaneous)
            
            delta = self._calc_delta(beta, r, _gamma,
                                     current_prediction=current_prediction,
                                     bootstrap_prediction=bootstrap_prediction)
            
            self._update_traces(traces=self.traces, gamma=_gamma, _lambda=self.l, indices=self.last_indices, alpha=_alpha)
            self._update_weights(weights=self.weights, alpha=_alpha, delta=delta, current_prediction=current_prediction, 
                                last_prediction=self.last_value, 
                                traces=self.traces, 
                                last_indices=self.last_indices)
            
            
            self.last_value = bootstrap_prediction
        self.last_indices = feature_indices
        
        return delta